# Secret Escapes Front-End Test
by Dmytro Poliyivets.


### Quick Note
> The API is very bad and does not allow to return the data
> for top 500 stories. Instead it returns ID's of those
> stories. That is why I fetch stories by ID one-by-one.


### Preview
If you don't want to download the repo and install the dependencies, you can preview the result on Netlify [secret-escapes-front-end-test.netlify.com](https://secret-escapes-front-end-test.netlify.com/)

### Installation

You’ll need to have Node 8.16.0 or Node 10.16.0 or later version on your local machine to run this code.

Install the dependencies and start the server.

```sh
$ cd secret-escapes-front-end-test
$ npm install
$ npm start
```

### Tech

* [ReactJS]
* [styled-components]
* [ant.design]
* [Redux]
* [redux-thunk]
* [Fluid typography]

### Resources

* [Flat Icon] – to minimise size compared to importing all icons from Ant.design
* [Unsplash]
* [Google Fonts]

[ReactJS]: <https://reactjs.org/>
[styled-components]: <https://www.styled-components.com/>
[ant.design]: <https://ant.design/>
[Redux]: <https://redux.js.org/introduction/getting-started/>
[redux-thunk]: <https://github.com/reduxjs/redux-thunk/>
[Fluid typography]: <https://css-tricks.com/snippets/css/fluid-typography/>
[Flat Icon]: <https://www.flaticon.com/home>
[Unsplash]: <https://unsplash.com/>
[Google Fonts]: <https://fonts.google.com/>