// Import dependencies
import React from 'react'

// Import components
import { Button } from 'antd'
import ScrollDown from './ScrollDown'
import { ParagraphText, HeaderTitleText } from '../../styled-components/UILibrary'

// Import styles
import './Header.css'

// Import images
import logo from '../../assets/svg/logo.svg'
import bookmark from '../../assets/svg/bookmark.svg'

/*
    Header component
*/
const Header = () => {
   return (
      <header className='header__container'>
         {/* Top Panel */}
         <div className='header__top-container'>
            <img src={logo} className='header-logo' alt='logo' />
            <div className='header__top-panel-info'>
               <ParagraphText colour={'#fff'} textAlign='right'>
                  by Dmitry Poliyivets
               </ParagraphText>
               <ParagraphText colour={'#8A919A'} textAlign='right' fontSize={'10px'}>
                  for Secret Escapes
               </ParagraphText>
            </div>
         </div>

         {/* Title */}
         <div className='header__title'>
            <HeaderTitleText>
               Stay on top
               <br />
               of the latest
               <br />
               tech news
            </HeaderTitleText>
         </div>

         {/* Action Component */}
         <div className='header__action-container'>
            <ScrollDown />
            <Button type='primary' size='large' href='#card-grid'>Explore now</Button>
            <div className='header__bookmark-container'>
               <img src={bookmark} className='header__bookmark-icon' alt='bookmark' />
            </div>
         </div>
      </header>
   )
}

export default Header
