// Import dependencies
import React from 'react'

// Import components
import { Avatar, Tag } from 'antd'
import { ParagraphText, TitleText } from '../../styled-components/UILibrary'

// Import styles
import './StoryCard.css'

// Import images
import scoreIcon from '../../assets/svg/up-arrow.svg'


/*
    Individual story card component
*/
export default class StoryCard extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         expanded: false,
         expandStyle: ''
      }
      this.toggleCard = this.toggleCard.bind(this)
   }

   /*
      Toggles a card on click
   */
   async toggleCard() {
      if (this.state.expanded) {
         await this.setState({ expandStyle: '', expanded: false })
         this.props.updateGrid()
      } else {
         await this.setState({ expandStyle: ' story-card__expanded', expanded: true })
         this.props.updateGrid()
      }
   }

   render() {
      // Get data
      const { author, title, desciption, score, image, type } = this.props

      return (
         <div className={'story-card__container' + this.state.expandStyle} onClick={this.toggleCard}>
            <div className='story-card__thumb-container'>
               <div className='story-card__tag'>
                  <Tag color='#1F2125'>{type}</Tag>
               </div>
               <img src={require('../../assets/images/thumbs/' + image + '-min.jpg')} alt='story-thumb' className='story-card__thumb-image' />
            </div>

            <div className='story-card__content'>
               <div className='story-card__avatar'>
                  <Avatar icon='user' style={{ backgroundColor: '#1F2125' }} size={33} />
               </div>

               <ParagraphText fontSize='9px' margin='margin-bottom: 10px !important'>
                  {'by ' + author}
               </ParagraphText>

               <div className={!this.state.expanded ? 'story-card__title-container' : null}>
                  <TitleText>{title}</TitleText>
               </div>

               <div className={!this.state.expanded ? 'story-card__description-container' : null}>
                  <ParagraphText margin='margin-top: 10px !important'>{desciption}</ParagraphText>
               </div>

               <div className='story-card__score-container'>
                  <ParagraphText fontSize='9px'>{score}</ParagraphText>
                  <img src={scoreIcon} alt='score' className='story-card__score-icon' />
               </div>
            </div>
         </div>
      )
   }
}

// Default props
StoryCard.defaultProps = {
   desciption:
      'No description provided. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
}
