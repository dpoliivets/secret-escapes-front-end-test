// Import dependencies
import React from 'react'
import StackGrid from 'react-stack-grid'
import ReactResizeDetector from 'react-resize-detector'

// Import components
import { Pagination } from 'antd'
import StoryCard from '../story-card/StoryCard'


/*
    Paginated Grid component
*/
export default class PaginatedGrid extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         displayData: [],
         columWidth: '25%'
      }
      this.updateData = this.updateData.bind(this)
   }

   /*
      Updates state depending on current page
   */
   updateData(start, end) {
      let result = this.props.data.slice(start, end)
      this.setState({ displayData: result })
   }

   /*
      Callback on change of page in pagination
   */
   onPageChange = (page, pageSize) => {
      let start = (page - 1) * pageSize
      let end = start + pageSize
      this.updateData(start, end)
   }

   /*
      Callback on change of page size in pagination
   */
   onPageSizeChange = (current, size) => {
      let start = (current - 1) * size
      let end = start + size
      this.updateData(start, end)
   }

   /*
      When the size of the StoryCard is changed
   */
   updateGrid = () => {
      this.grid.updateLayout()
   }

   /*
      Callback on window resize for responsiveness
   */
   onResize = (width) => {
      if (width > 850) {
         this.setState({ columWidth: '25%' })
      }
      if (width < 850) {
         this.setState({ columWidth: '33.333%' })
      }
      if (width < 680) {
         this.setState({ columWidth: '50%' })
      }
      if (width < 500) {
         this.setState({ columWidth: '100%' })
      }

      this.updateGrid()
   }

   componentDidMount() {
      this.updateData(0, 10)
   }

   /*
      Render grid initially for a 10 item/page layout
   */
   render() {
      // Get data
      const { totalItems } = this.props

      return (
         <ReactResizeDetector handleWidth onResize={this.onResize}>
            <StackGrid
               columnWidth={this.state.columWidth}
               gutterWidth={15}
               gutterHeight={15}
               monitorImagesLoaded
               style={{ display: 'flex', marginBottom: 40 }}
               gridRef={(grid) => (this.grid = grid)}
               duration={200}
               key='stack-grid'
            >
               {this.state.displayData.map((data, index) => (
                  <StoryCard
                     key={data.id}
                     author={data.by}
                     title={data.title}
                     description={data.text}
                     score={data.score}
                     image={(index + 1) % 10}
                     updateGrid={this.updateGrid}
                     type={data.type}
                  />
               ))}
            </StackGrid>

            <Pagination
               showSizeChanger
               hideOnSinglePage
               defaultCurrent={1}
               total={totalItems}
               onChange={this.onPageChange}
               onShowSizeChange={this.onPageSizeChange}
               key='pagination'
            />
         </ReactResizeDetector>
      )
   }
}
