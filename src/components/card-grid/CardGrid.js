// Import dependencies
import React from 'react'

// Import components
import { Spin } from 'antd'
import PaginatedGrid from './PaginatedGrid'
import { ContentLayout } from '../../styled-components/UILibrary'

// Import styles
import './CardGrid.css'

// Import redux
import { connect } from 'react-redux'
import { getTop500Data } from '../../redux/actions/top500Action'


/*
    Card Grid component
*/
class CardGrid extends React.Component {
   /*
        Fetch Top 500 stories data from Hacker News API
   */
   componentDidMount() {
      this.props.dispatch(getTop500Data())
   }

   render() {
      // Get data
      const { data } = this.props

      return (
         <ContentLayout id='card-grid'>
            {data.length < 11 ? (
               <div className='card-grid__loader'>
                  <Spin size='large' />
               </div>
            ) : (
               <PaginatedGrid totalItems={data.length} data={data} />
            )}
         </ContentLayout>
      )
   }
}

// Default props
CardGrid.defaultProps = {
   data: []
}

// Connect redux to component
const mapStateToProps = (state) => ({
   data: state.top500.data,
   isDataLoading: state.top500.isDataLoading,
   loadingFailed: state.top500.loadingFailed
})
export default connect(mapStateToProps)(CardGrid)
