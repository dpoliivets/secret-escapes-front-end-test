// Import dependencies
import styled from 'styled-components'

// Import constants
import { Constants } from '../constants/Constants'


/*
    Page layout components
*/
export const PageContainer = styled.div`
    width: 100vw;
    height: auto;
    background-color: #000000;
`;
export const ContentLayout = styled.main`
    width: 100%;
    padding-left: 12%;
    padding-right: 12%;
    margin-top: 100px;
`;


/*
    Fluid text components
*/
export const HeaderTitleText = styled.h1`
    font-family: 'Orbitron', sans-serif;
    font-weight: 700;
    font-size: calc(${ Constants.minHeaderTitle }px + (${ Constants.maxHeaderTitle } - ${ Constants.minHeaderTitle }) * ((100vw - 300px) / (1600 - 300)));
    line-height: 1.5;
    letter-spacing: 6px;
    color: ${ Constants.headerFontColour };
    margin: 0 !important;
    text-shadow: 4px 4px 2px #000000;
`;
export const TitleText = styled.h1`
    font-family: 'Orbitron', sans-serif;
    font-weight: 700;
    font-size: calc(${ Constants.minTitle }px + (${ Constants.maxTitle } - ${ Constants.minTitle }) * ((100vw - 300px) / (1600 - 300)));
    line-height: 1.5em;
    letter-spacing: 1px;
    color: ${ Constants.titleColour };
    margin: 0 !important;
`;
export const ParagraphText = styled.p`
    font-family: 'Montserrat', sans-serif;
    font-weight: 400;
    font-size: calc(${ Constants.minParagraphText }px + (${ Constants.maxParagraphText } - ${ Constants.minParagraphText }) * ((100vw - 300px) / (1600 - 300)));
    line-height: normal;
    color: ${ Constants.fontColour };
    margin: 0 !important;
    ${ props => props.colour ? 'color: '+props.colour : ''};
    ${ props => props.textAlign ? 'text-align: '+props.textAlign : ''};
    ${ props => props.fontSize ? 'font-size: '+props.fontSize : ''};
    ${ props => props.margin ? props.margin : ''};
`;
