// Import dependencies
import React from 'react'

// Import components
import Header from '../components/header/Header'
import Footer from '../components/footer/Footer'
import CardGrid from '../components/card-grid/CardGrid'
import { PageContainer } from '../styled-components/UILibrary'


/*
   Main Layout Component
*/
const NewsFeedLayout = () => {
   return (
      <PageContainer>
         {/* Header */}
         <Header />

         {/* Page Content */}
         <CardGrid />

         {/* Footer */}
         <Footer />
      </PageContainer>
   )
}

export default NewsFeedLayout
