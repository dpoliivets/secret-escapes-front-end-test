// Import axios
import axios from 'axios'


/*
    Instantiate a default Hacker News API with base URL
*/
const hackerNewsAPI = axios.create({
   baseURL: 'https://hacker-news.firebaseio.com/v0/'
})

export default hackerNewsAPI
