/*
    Import action types for Doctor Who data fetching
*/
import { FETCH_TOP_500_BEGIN, FETCH_TOP_500_SUCCESS, FETCH_TOP_500_FAILURE } from '../actions/top500Action'

/*
    Define default state
*/
let defaultState = {
   data: [],
   isDataLoading: true,
   loadingFailed: false
}

/*
    Define Doctor Who reducer
*/
const top500Reducer = (state, action) => {
   switch (action.type) {
      case FETCH_TOP_500_BEGIN:
         return {
            isDataLoading: true
         }
      case FETCH_TOP_500_SUCCESS:
         // Check if data was already added to the state
         if (state.data) {                        
            return {
               isDataLoading: false,
               data: [...state.data, action.payload]
            }
         }
         else {
            return {
               isDataLoading: false,
               data: [action.payload]
            }
         }
      case FETCH_TOP_500_FAILURE:
         return {
            isDataLoading: true,
            loadingFailed: true
         }
      default:
         return defaultState
   }
}

export default top500Reducer
