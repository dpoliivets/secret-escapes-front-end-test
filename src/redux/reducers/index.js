import { combineReducers } from 'redux'
import top500 from './top500Reducer'

export default combineReducers({
   top500
})
