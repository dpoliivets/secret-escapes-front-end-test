// Import API
import hackerNewsAPI from '../../api/HackerNewsAPI'

/*
    Define action types for Top 500 stories data fetching
*/
export const FETCH_TOP_500_BEGIN = 'FETCH_TOP_500_BEGIN'
export const FETCH_TOP_500_SUCCESS = 'FETCH_TOP_500_SUCCESS'
export const FETCH_TOP_500_FAILURE = 'FETCH_TOP_500_FAILURE'

/*
    Action for pulling data from Hacker News API
*/
export function getTop500Data() {
   return (dispatch) => {
      // Dipatch begin event
      dispatch(fetchTop500Begin())
      hackerNewsAPI
         .get('/topstories.json')
         .then((response) => {
            // The API is very bad and does not allow to return the data
            // for top 500 stories. Instead it returns ID's of those
            // stories. That is why I have to fetch stories by ID one
            // by one.
            for (let i = 0; i < response.data.length; i++) {
               hackerNewsAPI
                  .get('/item/'+response.data[i]+'.json')
                  .then((response) => {
                     dispatch(fetchTop500Success(response.data))
                  })
                  .catch((error) => {
                     // Do noting...
                  })
            }
         })
         .catch((error) => {
            dispatch(fetchTop500Failure())
         })
   }
}

export const fetchTop500Begin = () => ({
   type: FETCH_TOP_500_BEGIN
})
export const fetchTop500Success = (data) => ({
   type: FETCH_TOP_500_SUCCESS,
   payload: data
})
export const fetchTop500Failure = () => ({
   type: FETCH_TOP_500_FAILURE
})
