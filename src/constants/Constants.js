/*
    This file contains constants used
    throughout the web app
*/
export const Constants = {
    // Font Colours
    fontColour: '#8A919A',
    headerFontColour: '#FFFFFF',
    titleColour: '#FFFFFF',
    // Font Sizes
    minParagraphText: 10,
    maxParagraphText: 12,
    minHeaderTitle: 30,
    maxHeaderTitle: 56,
    minTitle: 11,
    maxTitle: 13,
    // Website colours
    mainColour: '#FF0053'
};