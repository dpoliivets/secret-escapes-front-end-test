// Import dependencies
import React from 'react'

// Import components
import NewsFeedLayout from './layouts/NewsFeedLayout'

// Import styles
import './App.css'

function App() {
   return <NewsFeedLayout />
}

export default App
